#' create_key_figures_avis
#'
#' Crée un dataframe de chiffres-clés sur les avis pour les différents types de
#' consultation.
#'
#' @description Cette fonction prend en entrée un dataframe export_garance
#' contenant des données sur les consultations et un entier select_annee
#' représentant l'année pour laquelle vous souhaitez calculer les chiffres-clés.
#'
#' @param export_garance Un dataframe contenant les données des consultations.
#' @param df_type_consultation Un dataframe contenant les types de consultation
#' @param select_annee L'année pour laquelle vous souhaitez calculer les
#' chiffres-clés.
#'
#' @return Un dataframe contenant les chiffres-clés pour les différents types
#' de consultation.
#'
#' @import dplyr
#' @importFrom datalibaba importer_data
#' @importFrom tidyr replace_na
#'
#' @export
create_key_figures_avis <- function(export_garance,
                                    df_type_consultation,
                                    select_annee) {

  # Supprimer le libellé des types de consultation
  df_type_consultation <- df_type_consultation |> dplyr::select(-libelle)

  # Vérifier l'existence du dataframe export_garance
  if (is.null(export_garance)) {
    return(NULL)
  } else {
    # Calculer les chifre-clés sur les avis de l'année en cours
    key_figures_avis <- export_garance |>
      dplyr::filter(annee == select_annee) |>
      dplyr::group_by(type_consultation) |>
      dplyr::summarise(
        nb_avis = dplyr::n(),
        nb_avis_signes = sum(!is.na(date_final)),
        nb_avis_tacites = sum(!is.na(date_tacite)),
        tx_avis_soumis_ee = round(sum(!is.na(soumission_ee)) /
                                    dplyr::n() * 100, 2)
      ) |>
      dplyr::full_join(df_type_consultation,
                       by = c("type_consultation" = "identifiant")) |>
      dplyr::arrange(type_consultation) |>
      tidyr::replace_na(list(
        nb_avis = 0,
        nb_avis_signes = 0,
        nb_avis_tacites = 0,
        tx_avis_soumis_ee = 0)
      )

    return(key_figures_avis)
  }
}

#' create_key_figures_new_decisions
#'
#' Crée un dataframe de chiffres-clés sur les nouvelles décisions pour les
#' différents types de consultation.
#'
#' @description Cette fonction prend en entrée un dataframe export_garance
#' contenant des données sur les consultations et un entier select_annee
#' représentant l'année pour laquelle vous souhaitez calculer les chiffres-clés.
#'
#' @param export_garance Un dataframe contenant les données des types de
#' consultation.
#' @param df_type_consultation Un dataframe contenant les types de consultation
#' @param select_annee L'année pour laquelle vous souhaitez calculer les
#' chiffres-clés.
#'
#' @return Un dataframe contenant les chiffres-clés pour les différents types
#' de consultation.
#'
#' @import dplyr
#' @importFrom datalibaba importer_data
#' @importFrom tidyr replace_na
#'
#' @export
create_key_figures_new_decisions <- function(export_garance,
                                             df_type_consultation,
                                             select_annee) {

  # Supprimer le libellé des types de consultation
  df_type_consultation <- df_type_consultation |> dplyr::select(-libelle)

  # Vérifier l'existence du dataframe export_garance
  if (is.null(export_garance)) {
    return(NULL)
  } else {
    # Calculer les chifre-clés sur les avis de l'année en cours
    key_figures_new_decisions <-  export_garance |>
      dplyr::filter(annee_nouvelle_decision == select_annee) |>
      dplyr::group_by(type_consultation) |>
      dplyr::summarise(
        nb_nouvelle_decision = sum(!is.na(date_nouvelle_decision))
      ) |>
      dplyr::full_join(df_type_consultation,
                       by = c("type_consultation" = "identifiant")) |>
      dplyr::arrange(type_consultation) |>
      tidyr::replace_na(list(nb_nouvelle_decision = 0))

    return(key_figures_new_decisions)
  }
}

#' create_tx_decisions_adhoc
#'
#' Calcul du taux de soumission pour les AEPP-CC-ADHOC
#'
#' @description Cette fonction calcule le taux de soumission pour les
#' AEPP-CC-ADHOC dans un dataframe en spécifiant une année.
#'
#' @param data Un data frame contenant les données de suivi des dossiers.
#' @param select_annee L'année sélectionnée, pour laquelle le taux de soumission
#' doit être calculé.
#'
#' @return Le taux de soumission pour les AEPP-CC-ADHOC pour l'année spécifiée.
#'
#' @importFrom dplyr filter
#'
#' @export
create_tx_decisions_adhoc <- function(data,
                                      select_annee) {
  # Filtrer les données en deux étapes
  aepp_cc_adhoc_annee  <- data |>
    dplyr::filter(type_consultation == "AEPP-CC-ADHOC", annee == select_annee)

  df_filtre <- aepp_cc_adhoc_annee  |>
    dplyr::filter(soumission_ee == "oui", statut != "avis sans observation")

  # Calculer le nombre de décisions soumises
  nb_soumis <- nrow(df_filtre)

  # Filtrer les données pour calculer le nombre de décisions émises
  emis <- aepp_cc_adhoc_annee  |>
    dplyr::filter(!is.na(date_final))

  # Calculer le nombre de décisions émises
  nb_emis <- nrow(emis)

  # Calculer le taux de soumission et l'arrondir à 2 décimales, en tenant
  # compte de nb_emis
  tx_decisions_adhoc <- ifelse(nb_emis == 0, 0,
                               round((nb_soumis / nb_emis) * 100, 2))

  return(tx_decisions_adhoc)
}
