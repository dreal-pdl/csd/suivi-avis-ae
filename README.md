
<!-- README.md is generated from README.Rmd. Please edit that file -->

# suiviAvisAE

<!-- badges: start -->

[![License:
MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
<!-- badges: end -->

Le package `suiviAvisAE` permet d’installer l’application de **Suivi des
avis de l’Autorité environnementale en Pays de la Loire**

## Installation

Vous pouvez installer la dernière version du package `suiviAvisAE` de
cette façon :

``` r
remotes::install_local("suiviAvisAE_0.0.0.9000.tar.gz")
```

``` r
devtools::install_git(url = "https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/suivi-avis-ae.git")
```

## Chargement

``` r
library(suiviAvisAE)
```

## Fonctionnalités

L’application permet d’afficher les données de suivi des avis et
décisions de l’Autorité environnementable en Pays de la Loire.

Elle est accessible via l’Intranet de la DREAL et propose les rubriques
suivantes :

### Chiffres-clés

Sélection d’indicateurs annuels par type de consultation affichés en
page d’accueil.

### Bilans d’activité

Affichage des bilans d’activité par département, type de consultation,
année et trimestre :

- recherche plein texte,
- tri par colonne,
- filtre par colonne,
- export des données par :
  - copie dans le presse-papiers,
  - téléchargement d’un fichier au format .xlsx.

### Statistiques

- Affichage de tableaux statistiques par type de consultation, par
  trimestre et annuellement

### Graphiques

- Affichage de graphiques présentant des analyses :
  - par département,
  - par domaine,
  - par catégorie.

### Évolutions

Affichage de graphiques présentant l’évolution du nombre de dossiers par
type de consultation : - graphiques de répartition par type d’avis
(exprès ou tacite) pour les AEPP et AEPR, - graphiques de répartition
par type de décision (soumission/dispense) pour les cas par cas, - par
millésime pour les années précédentes, - par mois pour l’année
sélectionnée.

Les items de la légende peuvent être sélectionnés pour masquer/afficher
les séries pour chaque graphique.

### Cartes

Cartographies des données géolocalisées par département et par année
pour : - les décisions de l’autorit0e9 environnementale sur les dossiers
d’examen au cas par cas (AEPR-CC) - les avis de l’autorit0e9
environnementale sur les projets soumis 0e0 0e9tude d’impact (AEPR)

### Aide

- Présentation de l’application
- Glossaire

### Développement

- Journal des modifications
- Feuille de route

### Mentions légales

### Nous contacter

Lien vers la boîte aux lettres fonctionnelle ADL.
