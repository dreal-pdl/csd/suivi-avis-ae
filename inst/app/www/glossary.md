# Glossaire

## Autorité environnementale

Le ministère de la transition écologique et solidaire est responsable, dans le cadre des directives européennes, de la définition et du suivi de la mise en œuvre de la politique nationale en matière d’évaluation environnementale des projets des plans/programmes et des documents d’urbanisme.
Dans ce cadre, **« l’autorité compétente en matière d’environnement »** : l’autorité environnementale rend des avis (évaluation environnementale systématique) ou des décisions (après un examen au cas par cas) sur les projets, plans/programmes et documents d’urbanisme susceptibles d’avoir des incidences notables sur l’environnement et sur les mesures de gestion visant à éviter, atténuer ou compenser ces impacts.

## Évaluation environnementale 

L’évaluation environnementale est une démarche qui vise à intégrer le plus en amont possible les préoccupations d’environnement dans l’élaboration des projets, des plans et programmes et des documents d’urbanisme, afin de favoriser le développement durable du territoire 

## Étude d’impact

Processus d'évaluation environnementale des projets de travaux et d’aménagements afin d'évaluer leur impact sur l'environnement. Le contenu de l'étude d'impact est fixé aux articles L.122-3 et R.122-4 du code de l'environnement.

## Plans/Programmes

**Les plans-programmes** sont des documents qui, dans un domaine particulier (eau, risques, patrimoine…), visent à planifier et à programmer un ensemble d’actions ou de projets sur un territoire. Cette planification peut être réalisée à l’échelle d’un État, d’une région, d’un bassin ou d’un sous-bassin, d’un département, d’une agglomération…

**Exemples :**

- schémas directeurs d’aménagement et de gestion des eaux (SDAGE) ;
- schémas d’aménagement et de gestion des eaux (SAGE) ; 
- schéma régional du climat, de l’air et de l’énergie (SRCAE) ;
- schéma régional de cohérence écologique (SRCE) ; 
- chartes de parcs naturels régionaux et des parcs nationaux ;
- plans de prévention des risques (PPR) naturels, miniers ou technologiques ;
- plans de déplacements urbains (PDU) ;
- aires de mise en valeur de l’architecture et du patrimoine (AVAP) ; 
- zonages d’assainissement eaux pluviales (ZAEP) ou eaux usées (ZAEU)… 

Font également partie des plans-programmes, **les documents d’urbanisme** :

- cartes communales (CC) ; 
- Plans locaux d’urbanisme (PLU) ;
- Schéma de cohérence territoriale (SCoT)
      
## Projets

**Les projets** sont des travaux, ouvrages ou aménagements (publics ou privés).

**Exemples :**

- installations classées pour la protection de l’environnement (ICPE) ; 
- infrastructures (routières, ferroviaires, aériennes…) ; 
- immobiliers (immeubles, zones d’aménagement concerté (ZAC)…) ; 
- équipements et d’aménagements touristiques (campings, pistes de ski…) ;  
- extensions ou de modifications d’aménagements existants… 

## Sigles

- AEPP : Autorité environnementale Plans/Programmes
- AEPP-CC : Autorité environnementale cas par cas Plans/Programmes
- AEPP-CC-ADHOC : Autorité environnementale cas par cas Plans/Programmes avis conformes
- AEPR : Autorité environnementale Projets
- AEPR-CC : Autorité environnementale cas par cas Projets
