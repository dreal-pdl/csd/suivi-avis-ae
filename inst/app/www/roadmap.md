# Feuille de route

## Version 1.2.3

* ajout de graphiques d'évolution par trimestre pour un suivi plus fin
* affichage d'un pourcentage (chiffre à rapporter au nombre de dossier "total"), pour les graphes de comparaisons des millésimes, sur les courbes concernant les tacites, les signés, les soumissions à EE et les dispenses d'EE plutôt que l'information d'un nombre de dossiers
* affichage de la carte des avis et décisions de l'autorité environnementale en Pays de la Loire  

----------
