# Présentation

L’application **« Autorité environnementale en Pays de la Loire »**  permet 
d'afficher par millésime (sélectionnable via une liste déroulante) les données 
de suivi des avis et décisions de l'Autorité environnementable en Pays de la 
Loire.

Elle est accessible via l’Intranet de la DREAL.

Cette application propose les rubriques suivantes :

## Chiffres-clés

Sélection d’indicateurs annuels par type de consultation affichés en page 
d'accueil.

## Bilans d’activité

Affichage des bilans d’activité par type de consultation, par trimestre et 
annuellement avec les fonctionnalités suivantes :

- recherche plein texte,
- tri par colonne,
- filtre par colonne,
- export des données par :
   - copie dans le presse-papiers,
   - impression,
   - téléchargement d'un fichier au format .xlsx. 

## Statistiques

- Affichage de tableaux statistiques par type de consultation, par trimestre 
et annuellement
- Affichage de graphiques présentant des analyses : 
	- par département,
	- par domaine,
	- par catégorie.
	
## Évolutions

Affichage de graphiques présentant l'évolution du nombre de dossiers par type 
de consultation :
- graphiques de répartition par type d'avis (exprès ou tacite) pour les AEPP et AEPR,
- graphiques de répartition par type de décision (soumission/dispense) pour les cas par cas,
- par millésime pour les années précédentes,
- par mois pour l'année sélectionnée.

Les items de la légende peuvent être sélectionnés pour masquer/afficher les 
séries pour chaque graphique.

## Aide

- Présentation de l’application
- Glossaire

## Développement

- Journal des modifications
- Feuille de route

## Mentions légales

## Nous contacter

Lien vers la boîte aux lettres fonctionnelle ADL.

----------
